module.exports = {
    "extends": "airbnb-base",
    "plugins": ["jest"],
    "env": {
      "jest/globals": true
    },
    "rules": {
      "no-use-before-define": ["off"],
			"no-unused-expressions": ["off"],
      "comma-dangle": ["off"],
      "no-continue": ["off"],
      "no-underscore-dangle": ["off"],
      "quotes": ["off"],
      "arrow-parens": ["off"],
      "no-plusplus": ["off"],
      "default-case": ["off"],

      /* Original Airbnb config prohibits these 3 features plus
         ForOfStatements. But ForOfStatements are awesome, so I'm
         manually resetting this rule to allow ForOfStatements.
         The other 3 features are still disabled, though. */
      "no-restricted-syntax": [ "error",
        "ForInStatement",
        "LabeledStatement",
        "WithStatement"
      ]
    }
};

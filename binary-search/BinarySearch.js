function binarySearch(array, targetVal) {
  let highIndex = array.length - 1;
  let lowIndex = 0;

  while (lowIndex <= highIndex) {
    const midIndex = Math.floor((lowIndex + highIndex) / 2);
    if (array[midIndex] === targetVal) {
      return midIndex;
    }

    if (array[midIndex] < targetVal) {
      lowIndex = midIndex + 1;
    } else {
      highIndex = midIndex - 1;
    }
  }

  return -1;
}

module.exports = binarySearch;

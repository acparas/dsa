const binarySearch = require('./BinarySearch');

describe('Binary Search', () => {
  test('Left-middle element typical array of odd length', () => {
    const arr = [-190, -68.28, -3, 0, 5, 150, 39200];
    const index = binarySearch(arr, -68.28);
    expect(index).toBe(1);
  });

  test('Right-middle on a typical array of even length', () => {
    const arr = [-284.32, -156, -1, 5, 79, 169.93, 183.89, 3005];
    const index = binarySearch(arr, 169.93);
    expect(index).toBe(5);
  });

  test('Exact middle on a typical array of odd length', () => {
    const arr = [-283, -22, 19, 89, 776, 39909, 29283484];
    const index = binarySearch(arr, 89);
    expect(index).toBe(3);
  });

  test('Math.floor((L + H) / 2) on a typical array of even length', () => {
    const arr = [-93746, -2557, -1463, -665, 0, 3];
    const index = binarySearch(arr, -1463);
    expect(index).toBe(2);
  });

  test('Array of all duplicates', () => {
    const arr = [0, 0, 0, 0, 0, 0, 0];
    const index = binarySearch(arr, 0);
    expect(index).toBe(3);
  });

  test('Left-most element on a typical array', () => {
    const arr = [-1432237, -65468, -9132, 34, 293, 19848];
    const index = binarySearch(arr, -1432237);
    expect(index).toBe(0);
  });

  test('Right-most element on a typical array', () => {
    const arr = [-3828, 0, 2, 3, 73.9, 889, 1093, 23476];
    const index = binarySearch(arr, 23476);
    expect(index).toBe(7);
  });

  test('DNE element on a typical array', () => {
    const arr = [5.38, 32.98, 65.96, 987.009, 34287555];
    const index = binarySearch(arr, 100);
    expect(index).toBe(-1);
  });

  test('empty array', () => {
    const arr = [];
    const index = binarySearch(arr, 2);
    expect(index).toBe(-1);
  });
});

class GraphNode {
  constructor(data) {
    this.data = data;
    this.neighbors = [];
  }
}

module.exports = GraphNode;

const Node = require('./GraphNode');

describe('GraphNode', () => {
  test('Creating a GraphNode with some data', () => {
    const node = new Node('some data');
    expect(node.data).toBe('some data');
  });

  test('Adding some neighbor nodes', () => {
    const nodeA = new Node('A');
    const nodeB = new Node('B');
    const nodeC = new Node('C');
    const nodeD = new Node('D');

    const aNeighbors = [nodeB, nodeC, nodeD];
    nodeA.neighbors.push(...aNeighbors);
    expect(nodeA.neighbors).toEqual(aNeighbors);

    const bNeighbors = [nodeA, nodeC, nodeD];
    nodeB.neighbors.push(...bNeighbors);
    expect(nodeB.neighbors).toEqual(bNeighbors);

    const cNeighbors = [nodeA, nodeB, nodeD];
    nodeC.neighbors.push(...cNeighbors);
    expect(nodeC.neighbors).toEqual(cNeighbors);

    const dNeighbors = [nodeA, nodeB, nodeC];
    nodeD.neighbors.push(...dNeighbors);
    expect(nodeD.neighbors).toEqual(dNeighbors);
  });
});

function bfs(visit, startNode) {
  const queue = [startNode];
  const visitedNodes = new Set();

  while (queue.length > 0) {
    const currentNode = queue.shift();
    if (visitedNodes.has(currentNode)) {
      continue;
    }

    visit(currentNode);
    visitedNodes.add(currentNode);

    queue.push(...currentNode.neighbors);
  }
}

module.exports = bfs;

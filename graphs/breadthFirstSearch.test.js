const bfs = require('./breadthFirstSearch');
const adjList1 = require('./fixtures/SimpleGraph1');
const adjList2 = require('./fixtures/SimpleGraph2');

describe('Breadth-first Search', () => {
  describe('Testing on SimpleGraph1', () => {
    test('starting @ node 0', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[0];
      bfs(visit, startNode);

      expect(trackingList).toEqual([
        0,
        1,
        9,
        8,
        7,
        3,
        6,
        11,
        10,
        2,
        4,
        5
      ]);
    });

    test('starting @ node 7', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[7];
      bfs(visit, startNode);

      expect(trackingList).toEqual([
        7,
        8,
        3,
        6,
        11,
        10,
        1,
        9,
        2,
        4,
        5,
        0
      ]);
    });

    test('starting @ node 12', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[12];
      bfs(visit, startNode);

      expect(trackingList).toEqual([12]);
    });
  });

  describe('Testing on SimpleGraph2', () => {
    test('starting @ node 8', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[8];
      bfs(visit, startNode);

      expect(trackingList).toEqual([
        8,
        1,
        9,
        12,
        10,
        0,
        2,
        7,
        11,
        3,
        6,
        4,
        5
      ]);
    });

    test('starting @ node 6', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[6];
      bfs(visit, startNode);

      expect(trackingList).toEqual([
        6,
        5,
        7,
        0,
        3,
        11,
        9,
        2,
        4,
        8,
        10,
        12,
        1
      ]);
    });
  });
});

function createVisitFunc(trackingList) {
  return function visit(node) {
    trackingList.push(node.data);
  };
}

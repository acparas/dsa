function checkIfNonDecreasingPathExists(grid, startPoint, endPoint) {
  const visitedPoints = {};
  return _dfs(startPoint);

  function _dfs(point) {
    if (isEndPoint(point)) {
      return true;
    }

    visitedPoints[point] = true;
    const validAdjacentPoints = getValidAdjacentPoints(point);
    const downstreamResults = validAdjacentPoints
      .map(validAdjPoint => _dfs(validAdjPoint));

    return downstreamResults.includes(true);
  }

  function isEndPoint(point) {
    return point[0] === endPoint[0] && point[1] === endPoint[1];
  }

  function getValidAdjacentPoints(point) {
    const [row, col] = point;
    const adjacentPoints = [];
    if (row - 1 >= 0) {
      adjacentPoints.push([row - 1, col]);
    }

    if (row + 1 < grid.length) {
      adjacentPoints.push([row + 1, col]);
    }

    if (col - 1 >= 0) {
      adjacentPoints.push([row, col - 1]);
    }

    if (col + 1 < grid[row].length) {
      adjacentPoints.push([row, col + 1]);
    }

    const unvisitedAdjPoints = adjacentPoints
      .filter(adjPoint => !visitedPoints[adjPoint]);

    const currentPointVal = grid[row][col];
    const validAdjPoints = unvisitedAdjPoints
      .filter(([adjRow, adjCol]) => grid[adjRow][adjCol] >= currentPointVal);

    return validAdjPoints;
  }
}

module.exports = checkIfNonDecreasingPathExists;

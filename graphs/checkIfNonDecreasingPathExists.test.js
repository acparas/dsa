/* eslint-disable indent */
/* eslint-disable function-paren-newline */
const checkIfNonDecreasingPathExists = require('./checkIfNonDecreasingPathExists');

describe('checkIfNonDecreasingPathExists()', () => {
  test(`grid = [1, 2, 2, 2],
             [0, 2, 2, 1],
             [0, 2, 2, 1],
             [3, 1, 3, 3]
        startPoint = [0, 0]
        endPoint = [3, 3]`,
        () => {
          const grid = [
            [1, 2, 2, 2],
            [0, 2, 2, 1],
            [0, 2, 2, 1],
            [3, 1, 3, 3]
          ];

          const startPoint = [0, 0];
          const endPoint = [3, 3];
          const result = checkIfNonDecreasingPathExists(
            grid,
            startPoint,
            endPoint
          );

          expect(result).toEqual(true);
        }
  );

  test(`grid = [1, 2, 2, 2],
             [0, 2, 2, 1],
             [0, 2, 2, 1],
             [3, 1, 3, 3]
        startPoint = [1, 1]
        endPoint = [3, 2]`,
        () => {
          const grid = [
            [1, 2, 2, 2],
            [0, 2, 2, 1],
            [0, 2, 2, 1],
            [3, 1, 3, 3]
          ];

          const startPoint = [1, 1];
          const endPoint = [3, 2];
          const result = checkIfNonDecreasingPathExists(
            grid,
            startPoint,
            endPoint
          );

          expect(result).toEqual(true);
        }
  );

  test(`grid = [1, 2, 2, 2],
             [0, 2, 2, 1],
             [0, 2, 2, 1],
             [3, 1, 3, 3]
        startPoint = [2, 3]
        endPoint = [0, 1]`,
        () => {
          const grid = [
            [1, 2, 2, 2],
            [0, 2, 2, 1],
            [0, 2, 2, 1],
            [3, 1, 3, 3]
          ];

          const startPoint = [2, 3];
          const endPoint = [0, 1];
          const result = checkIfNonDecreasingPathExists(
            grid,
            startPoint,
            endPoint
          );

          expect(result).toEqual(true);
        }
  );

  test(`grid = [1, 2, 2, 2],
             [0, 2, 2, 1],
             [0, 2, 2, 1],
             [3, 1, 3, 3]
        startPoint = [0, 3]
        endPoint = [3, 0]`,
        () => {
          const grid = [
            [1, 2, 2, 2],
            [0, 2, 2, 1],
            [0, 2, 2, 1],
            [3, 1, 3, 3]
          ];

          const startPoint = [0, 3];
          const endPoint = [3, 0];
          const result = checkIfNonDecreasingPathExists(
            grid,
            startPoint,
            endPoint
          );

          expect(result).toEqual(false);
        }
  );

  test(`grid = [3, 3, 3, 3],
             [3, 0, 0, 3],
             [3, 0, 0, 3],
             [3, 3, 3, 3]
        startPoint = [0, 0]
        endPoint = [2, 2]`,
        () => {
          const grid = [
            [3, 3, 3, 3],
            [3, 0, 0, 3],
            [3, 0, 0, 3],
            [3, 3, 3, 3]
          ];

          const startPoint = [0, 0];
          const endPoint = [2, 2];
          const result = checkIfNonDecreasingPathExists(
            grid,
            startPoint,
            endPoint
          );

          expect(result).toEqual(false);
        }
  );
});

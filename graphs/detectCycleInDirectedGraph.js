function detectCycleInDirectedGraph(nodes) {
  const nodesToStates = new Map();
  return nodes.some(_detectCycle);

  function _detectCycle(node) {
    switch (nodesToStates.get(node)) {
      case 'VISITED':
        return false;
      case 'PROCESSING':
        return true;
    }

    nodesToStates.set(node, 'PROCESSING');
    const hasCycle = node.neighbors.some(_detectCycle);
    nodesToStates.set(node, 'VISITED');

    return hasCycle;
  }
}

module.exports = detectCycleInDirectedGraph;

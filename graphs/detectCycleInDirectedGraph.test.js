const detectCycleInDirectedGraph = require('./detectCycleInDirectedGraph');
const adjList1 = require('./fixtures/DirectedAcyclicGraph1');
const adjList2 = require('./fixtures/DirectedCyclicGraph1');

describe('Detect cycles in directed graph', () => {
  test('Testing on DirectedAcyclicGraph1', () => {
    const result = detectCycleInDirectedGraph(adjList1);
    expect(result).toEqual(false);
  });

  test('Testing on DirectedCyclicGraph1', () => {
    const result = detectCycleInDirectedGraph(adjList2);
    expect(result).toEqual(true);
  });
});

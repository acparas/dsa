function detectCycleInUndirectedGraph(nodes) {
  const nodesToStates = new Map();
  return nodes.some(node => _dfs(null, node));

  function _dfs(prev, node) {
    switch (nodesToStates.get(node)) {
      case 'PROCESSING':
        return true;
      case 'VISITED':
        return false;
    }

    nodesToStates.set(node, 'PROCESSING');
    const hasCycle = node.neighbors
      .some(neighbor => neighbor !== prev && _dfs(node, neighbor));
    nodesToStates.set(node, 'VISITED');

    return hasCycle;
  }
}

module.exports = detectCycleInUndirectedGraph;

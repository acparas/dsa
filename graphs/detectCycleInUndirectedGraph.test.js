const detectCycleInUndirectedGraph = require('./detectCycleInUndirectedGraph');
const adjList1 = require('./fixtures/SimpleGraph1');
const adjList2 = require('./fixtures/SimpleGraph2');
const adjList3 = require('./fixtures/SimpleGraph3');
const adjList4 = require('./fixtures/SimpleGraph4');

describe('Detect cycles in undirected graph', () => {
  test('Testing on SimpleGraph1', () => {
    const result = detectCycleInUndirectedGraph(adjList1);
    expect(result).toEqual(true);
  });

  test('Testing on SimpleGraph2', () => {
    const result = detectCycleInUndirectedGraph(adjList2);
    expect(result).toEqual(true);
  });

  test('Testing on SimpleGraph3', () => {
    const result = detectCycleInUndirectedGraph(adjList3);
    expect(result).toEqual(true);
  });

  test('Testing on SimpleGraph4', () => {
    const result = detectCycleInUndirectedGraph(adjList4);
    expect(result).toEqual(false);
  });
});

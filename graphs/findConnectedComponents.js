function findConnectedComponents(nodesArr) {
  const componentsByLabel = new Map();
  let currentComponentLabel = 0;
  const visitedNodes = new Set();

  for (const node of nodesArr) {
    if (visitedNodes.has(node)) {
      continue;
    }

    const valsInComponent = [];
    componentsByLabel.set(currentComponentLabel, valsInComponent);

    _dfs(valsInComponent, node);
    currentComponentLabel++;
  }

  return componentsByLabel;

  function _dfs(valsInComponent, node) {
    if (visitedNodes.has(node)) {
      return;
    }

    valsInComponent.push(node.data);
    visitedNodes.add(node);

    node.neighbors.forEach(neighbor => _dfs(valsInComponent, neighbor));
  }
}

module.exports = findConnectedComponents;

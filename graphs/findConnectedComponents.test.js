const findConnectedComponents = require('./findConnectedComponents');
const nodesArr1 = require('./fixtures/SimpleGraph1');
const nodesArr2 = require('./fixtures/SimpleGraph2');
const nodesArr3 = require('./fixtures/SimpleGraph3');

describe('Find Connected Components', () => {
  test('Simple Graph 1', () => {
    const result = findConnectedComponents(nodesArr1);

    const component0 = result.get(0);
    expect(component0.sort((a, b) => a - b)).toEqual([
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11
    ]);

    const component1 = result.get(1);
    expect(component1.sort((a, b) => a - b)).toEqual([12]);
  });

  test('Simple Graph 2', () => {
    const result = findConnectedComponents(nodesArr2);

    const component0 = result.get(0);
    expect(component0.sort((a, b) => a - b)).toEqual([
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12
    ]);
  });

  test('Simple Graph 3', () => {
    const result = findConnectedComponents(nodesArr3);

    const component0 = result.get(0);
    expect(component0.sort((a, b) => a - b)).toEqual([0, 1, 2, 3, 4]);

    const component1 = result.get(1);
    expect(component1.sort((a, b) => a - b)).toEqual([5, 6]);

    const component2 = result.get(2);
    expect(component2.sort((a, b) => a - b)).toEqual([7, 8, 9]);
  });
});

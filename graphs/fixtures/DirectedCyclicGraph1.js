const Node = require('../GraphNode');

/* See DirectedCyclicGraph1.png to see what this is */
function createDirectedCyclicGraph1() {
  const nodes = [0, 1, 2, 3, 4, 5]
    .map(num => new Node(num));

  nodes[0].neighbors.push(nodes[1], nodes[2]);
  nodes[1].neighbors.push(nodes[2]);
  nodes[3].neighbors.push(nodes[0], nodes[4]);
  nodes[4].neighbors.push(nodes[5]);
  nodes[5].neighbors.push(nodes[3]);

  return nodes;
}

module.exports = createDirectedCyclicGraph1();

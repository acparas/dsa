const Node = require('../GraphNode');

/* See SimpleGraph3.png to see what this is */
function createSimpleGraph3() {
  const nodes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    .map(num => new Node(num));

  nodes[0].neighbors.push(nodes[1], nodes[3]);
  nodes[1].neighbors.push(nodes[0], nodes[2]);
  nodes[2].neighbors.push(nodes[1], nodes[3]);
  nodes[3].neighbors.push(nodes[0], nodes[2], nodes[4]);
  nodes[4].neighbors.push(nodes[3]);
  nodes[5].neighbors.push(nodes[6]);
  nodes[6].neighbors.push(nodes[5]);
  nodes[7].neighbors.push(nodes[8], nodes[9]);
  nodes[8].neighbors.push(nodes[7], nodes[9]);
  nodes[9].neighbors.push(nodes[7], nodes[8]);

  return nodes;
}

module.exports = createSimpleGraph3();

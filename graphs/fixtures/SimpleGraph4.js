const Node = require('../GraphNode');

/* See SimpleGraph4.png to see what this is */
function createSimpleGraph4() {
  const nodes = [0, 1, 2, 3, 4, 5]
    .map(num => new Node(num));

  nodes[0].neighbors.push(nodes[1], nodes[2]);
  nodes[1].neighbors.push(nodes[0], nodes[3], nodes[4]);
  nodes[2].neighbors.push(nodes[0]);
  nodes[3].neighbors.push(nodes[1]);
  nodes[4].neighbors.push(nodes[1]);

  return nodes;
}

module.exports = createSimpleGraph4();

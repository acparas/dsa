function dfs(visit, startNode) {
  const stack = [startNode];
  const visitedNodes = new Set();

  while (stack.length > 0) {
    const currentNode = stack.pop();
    if (visitedNodes.has(currentNode)) {
      continue;
    }

    visit(currentNode);
    visitedNodes.add(currentNode);

    stack.push(...currentNode.neighbors);
  }
}

module.exports = dfs;

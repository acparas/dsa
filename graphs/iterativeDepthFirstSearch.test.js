const dfs = require('./iterativeDepthFirstSearch');
const adjList1 = require('./fixtures/SimpleGraph1');
const adjList2 = require('./fixtures/SimpleGraph2');

describe('Iterative Depth-first Search', () => {
  describe('Testing on SimpleGraph1', () => {
    test('starting @ node 0', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[0];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        0,
        9,
        8,
        7,
        10,
        11,
        6,
        5,
        3,
        4,
        2,
        1
      ]);
    });

    test('starting @ node 7', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[7];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        7,
        10,
        11,
        6,
        5,
        3,
        4,
        2,
        8,
        9,
        0,
        1
      ]);
    });

    test('starting @ node 12', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[12];
      dfs(visit, startNode);

      expect(trackingList).toEqual([12]);
    });
  });

  describe('Testing on SimpleGraph2', () => {
    test('starting @ node 8', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[8];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        8,
        12,
        2,
        3,
        7,
        11,
        0,
        9,
        10,
        1,
        6,
        5,
        4
      ]);
    });

    test('starting @ node 6', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[6];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        6,
        7,
        11,
        0,
        9,
        10,
        1,
        8,
        12,
        2,
        3,
        4,
        5
      ]);
    });
  });
});

function createVisitFunc(trackingList) {
  return function visit(node) {
    trackingList.push(node.data);
  };
}

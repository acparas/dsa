function dfs(visitFunc, startNode) {
  const visitedNodes = new Set();
  _dfs(startNode);

  function _dfs(node) {
    if (visitedNodes.has(node)) {
      return;
    }

    visitFunc(node);
    visitedNodes.add(node);

    node.neighbors.forEach(_dfs);
  }
}

module.exports = dfs;

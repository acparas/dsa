const dfs = require('./recursiveDepthFirstSearch');
const adjList1 = require('./fixtures/SimpleGraph1');
const adjList2 = require('./fixtures/SimpleGraph2');

describe('Recursive Depth-first Search', () => {
  describe('Testing on SimpleGraph1', () => {
    test('starting @ node 0', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[0];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        0,
        1,
        8,
        9,
        7,
        3,
        2,
        4,
        5,
        6,
        11,
        10
      ]);
    });

    test('starting @ node 7', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[7];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        7,
        8,
        1,
        0,
        9,
        3,
        2,
        4,
        5,
        6,
        11,
        10
      ]);
    });

    test('starting @ node 12', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList1[12];
      dfs(visit, startNode);

      expect(trackingList).toEqual([12]);
    });
  });

  describe('Testing on SimpleGraph2', () => {
    test('starting @ node 8', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[8];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        8,
        1,
        10,
        9,
        0,
        7,
        3,
        2,
        12,
        4,
        6,
        5,
        11
      ]);
    });

    test('starting @ node 6', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[6];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        6,
        5,
        7,
        0,
        9,
        8,
        1,
        10,
        12,
        2,
        3,
        4,
        11
      ]);
    });
  });
  describe('Testing on SimpleGraph2', () => {
    test('starting @ node 8', () => {
      const trackingList = [];
      const visit = createVisitFunc(trackingList);

      const startNode = adjList2[8];
      dfs(visit, startNode);

      expect(trackingList).toEqual([
        8,
        1,
        10,
        9,
        0,
        7,
        3,
        2,
        12,
        4,
        6,
        5,
        11
      ]);
    });
  });
});

function createVisitFunc(trackingList) {
  return function visit(node) {
    trackingList.push(node.data);
  };
}

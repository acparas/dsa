function getMaximalIntervalSchedule(intervals) {
  intervals.sort((a, b) => a[1] - b[1]);
  const output = [];

  for (const interval of intervals) {
    if (output.length === 0 || interval[0] >= output[output.length - 1][1]) {
      output.push(interval);
    }
  }

  return output;
}

module.exports = getMaximalIntervalSchedule;

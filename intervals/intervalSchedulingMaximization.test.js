const getMaximalIntervalSchedule = require('./intervalSchedulingMaximization');

describe('getMaximalIntervalSchedule()', () => {
  const input1 = [
    [5, 6],
    [2, 4],
    [6, 7],
    [5, 7],
    [0, 4],
    [8, 10],
    [1, 2],
    [3, 6],
    [3, 5],
    [7, 9]
  ];

  test(format(input1), () => {
    const actualResult = getMaximalIntervalSchedule(input1);
    const correctResult = [
      [1, 2],
      [2, 4],
      [5, 6],
      [6, 7],
      [7, 9]
    ];

    expect(actualResult).toEqual(correctResult);
  });

  const input2 = [
    [4, 6],
    [1, 3],
    [6, 7],
    [5, 8],
    [7, 9],
    [2, 5]
  ];

  test(format(input2), () => {
    const actualResult = getMaximalIntervalSchedule(input2);
    const correctResult = [
      [1, 3],
      [4, 6],
      [6, 7],
      [7, 9]
    ];

    expect(actualResult).toEqual(correctResult);
  });

  const input3 = [
    [5, 7],
    [8, 9],
    [0, 1],
    [20, 31],
    [2, 3]
  ];

  test(format(input3), () => {
    const actualResult = getMaximalIntervalSchedule(input3);
    const correctResult = [
      [0, 1],
      [2, 3],
      [5, 7],
      [8, 9],
      [20, 31]
    ];

    expect(actualResult).toEqual(correctResult);
  });

  const input4 = [
    [0, 1],
    [0, 1],
    [0, 1],
    [0, 1],
    [0, 1]
  ];

  test(format(input4), () => {
    const actualResult = getMaximalIntervalSchedule(input4);
    const correctResult = [[0, 1]];
    expect(actualResult).toEqual(correctResult);
  });

  const input5 = [];
  test('[]', () => {
    const actualResult = getMaximalIntervalSchedule(input5);
    const correctResult = [];
    expect(actualResult).toEqual(correctResult);
  });
});

function format(arr) {
  let str = `${arr[0]}\n`;
  for (let i = 1; i < arr.length; i++) {
    str += `      ${arr[i]}\n`;
  }

  return str;
}

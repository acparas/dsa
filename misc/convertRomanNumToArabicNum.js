function convertRomanNumToArabicNum(romanNum) {
  const chars = Array.from(romanNum);
  let runningTotal = 0;

  let i = 0;
  while (i < chars.length) {
    const frontChar = chars[i];
    const frontVal = getValOfChar(frontChar);
    const backChar = chars[i + 1];
    const backVal = getValOfChar(backChar);

    if (backVal !== null && frontVal < backVal) {
      runningTotal += backVal - frontVal;
      i += 2;
    } else {
      runningTotal += frontVal;
      i++;
    }
  }

  return runningTotal;
}

function getValOfChar(char) {
  let val;
  switch (char) {
    case 'I':
      val = 1;
      break;
    case 'V':
      val = 5;
      break;
    case 'X':
      val = 10;
      break;
    case 'L':
      val = 50;
      break;
    case 'C':
      val = 100;
      break;
    case 'D':
      val = 500;
      break;
    case 'M':
      val = 1000;
      break;
    default:
      val = null;
  }

  return val;
}

module.exports = convertRomanNumToArabicNum;

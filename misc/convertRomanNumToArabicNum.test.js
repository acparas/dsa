const convertRomanNumToArabicNum = require('./convertRomanNumToArabicNum');

describe('convertRomanNumToArabicNum', () => {
  test('I', () => {
    const expectedResult = 1;
    const actualResult = convertRomanNumToArabicNum('I');
    expect(actualResult).toEqual(expectedResult);
  });

  test('V', () => {
    const expectedResult = 5;
    const actualResult = convertRomanNumToArabicNum('V');
    expect(actualResult).toEqual(expectedResult);
  });

  test('X', () => {
    const expectedResult = 10;
    const actualResult = convertRomanNumToArabicNum('X');
    expect(actualResult).toEqual(expectedResult);
  });

  test('L', () => {
    const expectedResult = 50;
    const actualResult = convertRomanNumToArabicNum('L');
    expect(actualResult).toEqual(expectedResult);
  });

  test('C', () => {
    const expectedResult = 100;
    const actualResult = convertRomanNumToArabicNum('C');
    expect(actualResult).toEqual(expectedResult);
  });

  test('D', () => {
    const expectedResult = 500;
    const actualResult = convertRomanNumToArabicNum('D');
    expect(actualResult).toEqual(expectedResult);
  });

  test('M', () => {
    const expectedResult = 1000;
    const actualResult = convertRomanNumToArabicNum('M');
    expect(actualResult).toEqual(expectedResult);
  });

  test('IV', () => {
    const expectedResult = 4;
    const actualResult = convertRomanNumToArabicNum('IV');
    expect(actualResult).toEqual(expectedResult);
  });

  test('VII', () => {
    const expectedResult = 7;
    const actualResult = convertRomanNumToArabicNum('VII');
    expect(actualResult).toEqual(expectedResult);
  });

  test('IX', () => {
    const expectedResult = 9;
    const actualResult = convertRomanNumToArabicNum('IX');
    expect(actualResult).toEqual(expectedResult);
  });

  test('XIX', () => {
    const expectedResult = 19;
    const actualResult = convertRomanNumToArabicNum('XIX');
    expect(actualResult).toEqual(expectedResult);
  });

  test('MCMXII', () => {
    const expectedResult = 1912;
    const actualResult = convertRomanNumToArabicNum('MCMXII');
    expect(actualResult).toEqual(expectedResult);
  });

  test('MMXIX', () => {
    const expectedResult = 2019;
    const actualResult = convertRomanNumToArabicNum('MMXIX');
    expect(actualResult).toEqual(expectedResult);
  });
});

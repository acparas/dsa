function generateAllMirrorNumbersOfLengthN(N) {
  if (N === 0) {
    return [];
  }

  if (N === 1) {
    return ['0', '1', '8'];
  }

  if (N === 2) {
    return ['00', '11', '88', '69', '96'];
  }

  const numsOf2IterationsAgo = generateAllMirrorNumbersOfLengthN(N - 2);
  const numsOfThisIteration = numsOf2IterationsAgo.flatMap(produceAllPaddings);

  return numsOfThisIteration;
}

function produceAllPaddings(num) {
  const leftPads = ['0', '1', '8', '6', '9'];
  const rightPads = ['0', '1', '8', '9', '6'];

  const paddings = [];
  for (let i = 0; i <= 4; i++) {
    paddings.push(leftPads[i] + num + rightPads[i]);
  }

  return paddings;
}

module.exports = generateAllMirrorNumbersOfLengthN;

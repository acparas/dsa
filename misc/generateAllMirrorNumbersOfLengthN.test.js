const generateAllMirrorNumbersOfLengthN = (
  require('./generateAllMirrorNumbersOfLengthN')
);

describe('generateAllMirrorNumbersOfLengthN()', () => {
  test('N = 0', () => {
    const expectedResult = [];
    expect(generateAllMirrorNumbersOfLengthN(0)).toEqual(expectedResult);
  });

  test('N = 1', () => {
    const expectedResult = ['0', '1', '8'];
    expect(generateAllMirrorNumbersOfLengthN(1)).toEqual(expectedResult);
  });

  test('N = 2', () => {
    const expectedResult = ['00', '11', '88', '69', '96'];
    expectedResult.sort();

    const actualResult = generateAllMirrorNumbersOfLengthN(2);
    actualResult.sort();

    expect(actualResult).toEqual(expectedResult);
  });

  test('N = 3', () => {
    const expectedResult = [
      '000',
      '101',
      '808',
      '609',
      '906',
      '010',
      '111',
      '818',
      '619',
      '916',
      '080',
      '181',
      '888',
      '689',
      '986'
    ];

    expectedResult.sort();

    const actualResult = generateAllMirrorNumbersOfLengthN(3);
    actualResult.sort();

    expect(actualResult).toEqual(expectedResult);
  });
});

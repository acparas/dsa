function getLongestJumpPath(nums) {
  const adjList = createAdjList(nums);
  /* we subtract 1 because the start node (-1) shouldn't be included
     in the length */
  return dfs(adjList, -1) - 1;
}

function createAdjList(nums) {
  const adjList = new Map();
  for (let i = 0; i < nums.length; i++) {
    const source = nums[i];
    const dest = i;

    !adjList.has(source) && adjList.set(source, []);
    !adjList.has(dest) && adjList.set(dest, []);

    const neighbors = adjList.get(source);
    neighbors.push(dest);
    adjList.set(source, neighbors);
  }

  return adjList;
}

function dfs(adjList, node) {
  const neighbors = adjList.get(node);
  return 1 + Math.max(...neighbors.map(neighbor => dfs(adjList, neighbor)), 0);
}

module.exports = getLongestJumpPath;

const getLongestJumpPath = require('./getLongestJumpPath');

describe('getLongestJumpPath()', () => {
  test('[5, 3, 7, -1, 2, 3, 1, 6]', () => {
    const input = [5, 3, 7, -1, 2, 3, 1, 6];
    const expectedResult = 6;
    const actualResult = getLongestJumpPath(input);
    expect(actualResult).toEqual(expectedResult);
  });

  test('[5, 3, 7, -1, 0, 4, 1, 6]', () => {
    const input = [5, 3, 7, -1, 0, 4, 1, 6];
    const expectedResult = 5;
    const actualResult = getLongestJumpPath(input);
    expect(actualResult).toEqual(expectedResult);
  });
});

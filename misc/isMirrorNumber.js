function isMirrorNumber(numAsStr) {
  const digits = Array.from(numAsStr).map(Number);
  const mid = Math.floor(digits.length / 2);
  for (let i = 0; i <= mid; i++) {
    const j = digits.length - 1 - i;
    const frontDigit = digits[i];
    const backDigit = digits[j];
    if (!isMirrorPair(frontDigit, backDigit)) {
      return false;
    }
  }

  return true;
}

function isMirrorPair(digitA, digitB) {
  return (
    (digitA === 0 && digitB === 0)
    || (digitA === 1 && digitB === 1)
    || (digitA === 8 && digitB === 8)
    || (digitA === 6 && digitB === 9)
    || (digitA === 9 && digitB === 6)
  );
}

module.exports = isMirrorNumber;

const isMirrorNumber = require('./isMirrorNumber');

describe('isMirrorNumber()', () => {
  test('0', () => {
    expect(isMirrorNumber('0')).toBe(true);
  });

  test('1', () => {
    expect(isMirrorNumber('1')).toBe(true);
  });

  test('2', () => {
    expect(isMirrorNumber('2')).toBe(false);
  });

  test('4', () => {
    expect(isMirrorNumber('4')).toBe(false);
  });

  test('6', () => {
    expect(isMirrorNumber('6')).toBe(false);
  });

  test('8', () => {
    expect(isMirrorNumber('8')).toBe(true);
  });

  test('9', () => {
    expect(isMirrorNumber('9')).toBe(false);
  });

  test('00', () => {
    expect(isMirrorNumber('00')).toBe(true);
  });

  test('69', () => {
    expect(isMirrorNumber('69')).toBe(true);
  });

  test('96', () => {
    expect(isMirrorNumber('96')).toBe(true);
  });

  test('88', () => {
    expect(isMirrorNumber('88')).toBe(true);
  });

  test('818', () => {
    expect(isMirrorNumber('818')).toBe(true);
  });

  test('283', () => {
    expect(isMirrorNumber('283')).toBe(false);
  });

  test('986986', () => {
    expect(isMirrorNumber('986986')).toBe(true);
  });

  test('986786', () => {
    expect(isMirrorNumber('986786')).toBe(false);
  });
});

const getParentIdx = nodeIdx => Math.floor((nodeIdx - 1) / 2);
const getLeftChildIdx = nodeIdx => 2 * nodeIdx + 1;
const getRightChildIdx = nodeIdx => 2 * nodeIdx + 2;

class PriorityQueue {
  constructor(compareFunc = (a, b) => a < b) {
    this._heap = [];
    this._compare = compareFunc;
  }

  add(...values) {
    for (const val of values) {
      this._heap.push(val);
      this._bubbleUp();
    }
  }

  _bubbleUp() {
    let nodeIdx = this.size() - 1;
    while (
      nodeIdx > 0
      && this._hasHigherPriority(nodeIdx, getParentIdx(nodeIdx))
    ) {
      this._swap(nodeIdx, getParentIdx(nodeIdx));
      nodeIdx = getParentIdx(nodeIdx);
    }
  }

  size() {
    return this._heap.length;
  }

  _hasHigherPriority(idx1, idx2) {
    return this._compare(
      this._heap[idx1],
      this._heap[idx2]
    );
  }

  _swap(idx1, idx2) {
    [this._heap[idx1], this._heap[idx2]] = [this._heap[idx2], this._heap[idx1]];
  }

  poll() {
    this._swap(0, this.size() - 1);
    const topVal = this._heap.pop();
    this._sinkDown();
    return topVal;
  }

  _sinkDown() {
    let nodeIdx = 0;
    while (this._hasLeftChild(nodeIdx)) {
      let higherPriorityChildIdx = getLeftChildIdx(nodeIdx);
      if (this._rightChildIsHigherPriorityChild(nodeIdx)) {
        higherPriorityChildIdx = getRightChildIdx(nodeIdx);
      }

      if (this._hasHigherPriority(nodeIdx, higherPriorityChildIdx)) {
        break;
      }

      this._swap(nodeIdx, higherPriorityChildIdx);
      nodeIdx = higherPriorityChildIdx;
    }
  }

  _hasLeftChild(nodeIdx) {
    return getLeftChildIdx(nodeIdx) < this.size();
  }

  _rightChildIsHigherPriorityChild(nodeIdx) {
    if (!this._hasRightChild(nodeIdx)) {
      return false;
    }

    return this._hasHigherPriority(
      getRightChildIdx(nodeIdx),
      getLeftChildIdx(nodeIdx)
    );
  }

  _hasRightChild(nodeIdx) {
    return getRightChildIdx(nodeIdx) < this.size();
  }

  peek() {
    return this._heap[0];
  }
}

module.exports = PriorityQueue;

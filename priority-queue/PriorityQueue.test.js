const PriorityQueue = require('./PriorityQueue');

describe('PriorityQueue (is min by default)', () => {
  test('adding [5, 1, 4, 2, 3], then polling them all off', () => {
    const pq = new PriorityQueue();
    pq.add(5, 1, 4, 2, 3);
    [1, 2, 3, 4, 5].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [-5, -4, -3, -2, -1], then polling them all off', () => {
    const pq = new PriorityQueue();
    pq.add(-5, -4, -3, -2, -1);
    [-5, -4, -3, -2, -1].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [2, 1, 0, -1, -2], then polling them all off', () => {
    const pq = new PriorityQueue();
    pq.add(2, 1, 0, -1, -2);
    [-2, -1, 0, 1, 2].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [0, 0, 0, 0, 0], then polling them all off', () => {
    const pq = new PriorityQueue();
    pq.add(0, 0, 0, 0, 0);
    [0, 0, 0, 0, 0].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [-2, 5, -9, 0, 0.25], polling 2 elements, adding -99, then polling 4 more elements', () => {
    const pq = new PriorityQueue();
    pq.add(-2, 5, -9, 0, 0.25);
    [-9, -2].forEach(num => expect(pq.poll()).toBe(num));

    pq.add(-99);
    [-99, 0, 0.25, 5].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [718, -235.58, 1021.2, -3739, 9238.01], polling 3 elements, adding 99999, then polling 3 more elements', () => {
    const pq = new PriorityQueue();
    pq.add(718, -235.58, 1021.2, -3739, 9238.01);
    [-3739, -235.58, 718].forEach(num => expect(pq.poll()).toBe(num));

    pq.add(99999);
    [1021.2, 9238.01, 99999].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [99, -99, 3.14159], then alternating between peeking and polling', () => {
    const pq = new PriorityQueue();
    pq.add(99, -99, 3.14159);
    expect(pq.peek()).toBe(-99);
    expect(pq.poll()).toBe(-99);
    expect(pq.peek()).toBe(3.14159);
    expect(pq.poll()).toBe(3.14159);
    expect(pq.peek()).toBe(99);
    expect(pq.poll()).toBe(99);
  });

  test('adding [5, 4, 3, 2, 1], then alternating between size-checks and polling', () => {
    const pq = new PriorityQueue();
    pq.add(5, 4, 3, 2, 1);
    expect(pq.size()).toBe(5);
    expect(pq.poll()).toBe(1);
    expect(pq.size()).toBe(4);
    expect(pq.poll()).toBe(2);
    expect(pq.size()).toBe(3);
    expect(pq.poll()).toBe(3);
    expect(pq.size()).toBe(2);
    expect(pq.poll()).toBe(4);
    expect(pq.size()).toBe(1);
    expect(pq.poll()).toBe(5);
  });

  test('adding [-8, 2, -4, 5, -19], then alternating between checking size, adding more numbers, and polling', () => {
    const pq = new PriorityQueue();
    pq.add(-8, 2, -4, 5, -19); // [-19, -8, -4, 2, 5]
    expect(pq.size()).toBe(5);

    pq.add(-2413, 2413); // [-2413, -19, -8, -4, 2, 5, 2413]
    expect(pq.size()).toBe(7);

    [-2413, -19, -8].forEach(num => expect(pq.poll()).toBe(num));
    // [-4, 2, 5, 2413]
    expect(pq.size()).toBe(4);

    pq.add(99999); // [-4, 2, 5, 2413, 99999]
    expect(pq.size()).toBe(5);

    const remainingNums = [-4, 2, 5, 2413, 99999];
    for (let i = 0; i < 5; i++) {
      expect(pq.size()).toBe(5 - i);
      expect(pq.poll()).toBe(remainingNums[i]);
    }
  });
});

describe('Max PriorityQueue', () => {
  const maxComparator = (a, b) => a > b;

  test('adding [5, 1, 4, 2, 3], then polling them all off', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(5, 1, 4, 2, 3);
    [5, 4, 3, 2, 1].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [-5, -4, -3, -2, -1], then polling them all off', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(-5, -4, -3, -2, -1);
    [-1, -2, -3, -4, -5].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [2, 1, 0, -1, -2], then polling them all off', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(2, 1, 0, -1, -2);
    [2, 1, 0, -1, -2].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [0, 0, 0, 0, 0], then polling them all off', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(0, 0, 0, 0, 0);
    [0, 0, 0, 0, 0].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [-2, 5, -9, 0, 0.25], polling 2 elements, adding -99, then polling 4 more elements', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(-2, 5, -9, 0, 0.25);
    [5, 0.25].forEach(num => expect(pq.poll()).toBe(num));

    pq.add(-99);
    [0, -2, -9, -99].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [718, -235.58, 1021.2, -3739, 9238.01], polling 3 elements, adding 99999, then polling 3 more elements', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(718, -235.58, 1021.2, -3739, 9238.01);
    [9238.01, 1021.2, 718].forEach(num => expect(pq.poll()).toBe(num));

    pq.add(99999);
    [99999, -235.58, -3739].forEach(num => expect(pq.poll()).toBe(num));
  });

  test('adding [99, -99, 3.14159], then alternating between peeking and polling', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(99, -99, 3.14159);
    expect(pq.peek()).toBe(99);
    expect(pq.poll()).toBe(99);
    expect(pq.peek()).toBe(3.14159);
    expect(pq.poll()).toBe(3.14159);
    expect(pq.peek()).toBe(-99);
    expect(pq.poll()).toBe(-99);
  });

  test('adding [5, 4, 3, 2, 1], then alternating between size-checks and polling', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(5, 4, 3, 2, 1);
    expect(pq.size()).toBe(5);
    expect(pq.poll()).toBe(5);
    expect(pq.size()).toBe(4);
    expect(pq.poll()).toBe(4);
    expect(pq.size()).toBe(3);
    expect(pq.poll()).toBe(3);
    expect(pq.size()).toBe(2);
    expect(pq.poll()).toBe(2);
    expect(pq.size()).toBe(1);
    expect(pq.poll()).toBe(1);
  });

  test('adding [-8, 2, -4, 5, -19], then alternating between checking size, adding more numbers, and polling', () => {
    const pq = new PriorityQueue(maxComparator);
    pq.add(-8, 2, -4, 5, -19); // [5, 2, -4, -8, -19]
    expect(pq.size()).toBe(5);

    pq.add(-2413, 2413); // [2413, 5, 2, -4, -8, -19, -2413]
    expect(pq.size()).toBe(7);

    [2413, 5, 2].forEach(num => expect(pq.poll()).toBe(num));
    // [-4, -8, -19, -2413]
    expect(pq.size()).toBe(4);

    pq.add(99999); // [99999, -4, -8, -19, -2413]
    expect(pq.size()).toBe(5);

    const remainingNums = [99999, -4, -8, -19, -2413];
    for (let i = 0; i < 5; i++) {
      expect(pq.size()).toBe(5 - i);
      expect(pq.poll()).toBe(remainingNums[i]);
    }
  });
});

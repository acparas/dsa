function mergeSort(arr) {
  if (arr.length <= 1) {
    return arr;
  }

  const mid = Math.floor(arr.length / 2);
  const arrA = mergeSort(arr.slice(0, mid));
  const arrB = mergeSort(arr.slice(mid));
  const combinedArr = [];

  while (arrA.length > 0 || arrB.length > 0) {
    if (arrA.length > 0 && arrB.length > 0) {
      if (arrA[0] < arrB[0]) {
        combinedArr.push(arrA.shift());
      } else {
        combinedArr.push(arrB.shift());
      }
    } else if (arrA.length > 0) {
      combinedArr.push(arrA.shift());
    } else {
      combinedArr.push(arrB.shift());
    }
  }

  return combinedArr;
}

module.exports = mergeSort;

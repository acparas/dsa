const mergeSort = require('./mergeSort');
const { numArrays } = require('./fixtures/SampleArrays');

describe('Merge Sort', () => {
  test('7 Integers', () => {
    const result = mergeSort(numArrays.ints1);
    const answer = [...numArrays.ints1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Integers', () => {
    const result = mergeSort(numArrays.ints2);
    const answer = [...numArrays.ints2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Positive Integers', () => {
    const result = mergeSort(numArrays.positiveInts1);
    const answer = [...numArrays.positiveInts1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Positive Integers', () => {
    const result = mergeSort(numArrays.positiveInts2);
    const answer = [...numArrays.positiveInts2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Negative Integers', () => {
    const result = mergeSort(numArrays.negativeInts1);
    const answer = [...numArrays.negativeInts1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Negative Integers', () => {
    const result = mergeSort(numArrays.negativeInts2);
    const answer = [...numArrays.negativeInts2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Real Numbers', () => {
    const result = mergeSort(numArrays.realNums1);
    const answer = [...numArrays.realNums1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Real Numbers', () => {
    const result = mergeSort(numArrays.realNums2);
    const answer = [...numArrays.realNums2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('All Duplicates, 6 Zeroes', () => {
    const result = mergeSort(numArrays.allDupes);
    const answer = [...numArrays.allDupes].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('Empty Array', () => {
    const result = mergeSort(numArrays.emptyArr);
    const answer = [...numArrays.emptyArr].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });
});

function quickSort(arr) {
  qs(0, arr.length - 1);

  function qs(start, end) {
    if (start >= end) {
      return;
    }

    const pivotIndex = partition(start, end);
    qs(start, pivotIndex - 1);
    qs(pivotIndex + 1, end);
  }

  function partition(start, end) {
    const pivot = arr[end];
    let splitIndex = start;
    for (let i = start; i <= end - 1; i++) {
      if (arr[i] <= pivot) {
        swap(i, splitIndex);
        splitIndex++;
      }
    }

    swap(end, splitIndex);
    return splitIndex;
  }

  function swap(i, j) {
    /* eslint-disable no-param-reassign */
    /* Without this directive, ESLint will complain about this line. */
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
}

module.exports = quickSort;

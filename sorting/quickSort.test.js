const quickSort = require('./quickSort');
const { numArrays } = require('./fixtures/SampleArrays');

describe('Quicksort', () => {
  test('7 Integers', () => {
    const copy = [...numArrays.ints1];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.ints1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Integers', () => {
    const copy = [...numArrays.ints2];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.ints2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Positive Integers', () => {
    const copy = [...numArrays.positiveInts1];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.positiveInts1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Positive Integers', () => {
    const copy = [...numArrays.positiveInts2];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.positiveInts2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Negative Integers', () => {
    const copy = [...numArrays.negativeInts1];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.negativeInts1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Negative Integers', () => {
    const copy = [...numArrays.negativeInts2];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.negativeInts2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('7 Real Numbers', () => {
    const copy = [...numArrays.realNums1];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.realNums1].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('8 Real Numbers', () => {
    const copy = [...numArrays.realNums2];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.realNums2].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('All Duplicates, 6 Zeroes', () => {
    const copy = [...numArrays.allDupes];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.allDupes].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });

  test('Empty Array', () => {
    const copy = [...numArrays.emptyArr];
    quickSort(copy);
    const result = copy;
    const answer = [...numArrays.emptyArr].sort((a, b) => a - b);
    expect(result).toEqual(answer);
  });
});

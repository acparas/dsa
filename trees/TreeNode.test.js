const TreeNode = require('./TreeNode');

test('Creating a TreeNode with val', () => {
  const treeNode = new TreeNode(3);
  expect(treeNode.val).toBe(3);
});

test('Creating a TreeNode with a left child', () => {
  const leftChildNode = new TreeNode(1);
  const rootNode = new TreeNode(0, leftChildNode);
  expect(rootNode.left).toBe(leftChildNode);
});

test('Creating a TreeNode with a right child', () => {
  const rightChildNode = new TreeNode(1);
  const rootNode = new TreeNode(0, null, rightChildNode);
  expect(rootNode.right).toBe(rightChildNode);
});

test('Creating a TreeNode with left and right children', () => {
  const leftChildNode = new TreeNode(1);
  const rightChildNode = new TreeNode(2);
  const rootNode = new TreeNode(0, leftChildNode, rightChildNode);
  expect(rootNode.left).toBe(leftChildNode);
  expect(rootNode.right).toBe(rightChildNode);
});

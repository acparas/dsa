/* eslint-disable prefer-destructuring */

const TreeNode = require('../TreeNode');

module.exports = {
  TYPICAL_BINARY_TREE1: createTypicalBinaryTree1(),
  TYPICAL_BINARY_TREE2: createTypicalBinaryTree2(),
  PERFECT_BINARY_TREE1: createPerfectBinaryTree1(),
  RIGHTWARD_DEGENERATE_BINARY_TREE1: createRightwardDegenerateBinaryTree1(),
  UNBALANCED_BINARY_TREE1: createUnbalancedBinaryTree1(),
  UNBALANCED_BINARY_TREE2: createUnbalancedBinaryTree2()
};

/*
              10
  |------------------------|
  5                        20
  -------|         |-----------------|
        12         3                 7
     |---      |-------|
     4         9       18
*/
function createTypicalBinaryTree1() {
  const node10 = new TreeNode(10);
  const node5 = new TreeNode(5);
  const node20 = new TreeNode(20);
  const node12 = new TreeNode(12);
  const node3 = new TreeNode(3);
  const node7 = new TreeNode(7);
  const node4 = new TreeNode(4);
  const node9 = new TreeNode(9);
  const node18 = new TreeNode(18);

  node10.left = node5;
  node10.right = node20;

  node5.right = node12;

  node20.left = node3;
  node20.right = node7;

  node12.left = node4;

  node3.left = node9;
  node3.right = node18;

  return node10;
}

/*
                      15
        |----------------------------|
        6                           23
  |-----------|                      -----|
  4           7                          71
  ----|                               |----
      5                              50
*/
function createTypicalBinaryTree2() {
  const nodes = {};
  const arr = [4, 5, 6, 7, 15, 23, 50, 71];

  for (const val of arr) {
    nodes[val] = new TreeNode(val);
  }

  nodes[15].left = nodes[6];
  nodes[15].right = nodes[23];
  nodes[6].left = nodes[4];
  nodes[6].right = nodes[7];
  nodes[23].right = nodes[71];
  nodes[4].right = nodes[5];
  nodes[71].left = nodes[50];

  return nodes[15];
}

/*
                    10
        |------------------------|
        5                        20
|---------------|         |-----------------|
9              18         3                 7

*/
function createPerfectBinaryTree1() {
  const node10 = new TreeNode(10);
  const node5 = new TreeNode(5);
  const node20 = new TreeNode(20);
  const node9 = new TreeNode(9);
  const node18 = new TreeNode(18);
  const node3 = new TreeNode(3);
  const node7 = new TreeNode(7);

  node10.left = node5;
  node10.right = node20;
  node5.left = node9;
  node5.right = node18;
  node20.left = node3;
  node20.right = node7;

  return node10;
}

/*
 1
 ----|
     2
     ----|
         3
         ----|
             4
             ----|
                 5
                 ----|
                     6
*/
function createRightwardDegenerateBinaryTree1() {
  const node1 = new TreeNode(1);
  const node2 = new TreeNode(2);
  const node3 = new TreeNode(3);
  const node4 = new TreeNode(4);
  const node5 = new TreeNode(5);
  const node6 = new TreeNode(6);

  node1.right = node2;
  node2.right = node3;
  node3.right = node4;
  node4.right = node5;
  node5.right = node6;

  return node1;
}

/*
              1
      |---------------|
      7               4
      --------|       ---------|
              2                6
      |---------------|
      3               9

*/

function createUnbalancedBinaryTree1() {
  const nodes = {};
  const arr = [1, 2, 3, 4, 6, 7, 9];

  for (const val of arr) {
    nodes[val] = new TreeNode(val);
  }

  nodes[1].left = nodes[7];
  nodes[1].right = nodes[4];
  nodes[7].right = nodes[2];
  nodes[2].left = nodes[3];
  nodes[2].right = nodes[9];
  nodes[4].right = nodes[6];

  return nodes[1];
}

/*
             6
|-------------------------|
2                         5
             |-------------------------|
             8                         2
             ---------|         |-------
                      9          3
                 |-----          ------|
                 1                     4
                                   |----
                                   5
*/
function createUnbalancedBinaryTree2() {
  const node1 = new TreeNode(1);
  const node2a = new TreeNode(2);
  const node2b = new TreeNode(2);
  const node3 = new TreeNode(3);
  const node4 = new TreeNode(4);
  const node5a = new TreeNode(5);
  const node5b = new TreeNode(5);
  const node6 = new TreeNode(6);
  const node8 = new TreeNode(8);
  const node9 = new TreeNode(9);

  node6.left = node2a;
  node6.right = node5a;
  node5a.left = node8;
  node5a.right = node2b;
  node8.right = node9;
  node2b.left = node3;
  node9.left = node1;
  node3.right = node4;
  node4.left = node5b;

  return node6;
}

function getColumnOrderOfBinaryTree(rootNode) {
  const columnsToNodeVals = new Map();
  let minCol = Infinity;
  let maxCol = -Infinity;

  const queue = [{ node: rootNode, column: 0 }];
  while (queue.length > 0) {
    const queueItem = queue.shift();
    if (!queueItem.node) {
      continue;
    }

    const { node, column } = queueItem;

    minCol = Math.min(minCol, column);
    maxCol = Math.max(maxCol, column);

    if (columnsToNodeVals.has(column)) {
      const nodeValsForThisCol = columnsToNodeVals.get(column);
      nodeValsForThisCol.push(node.val);
    } else {
      columnsToNodeVals.set(column, [node.val]);
    }

    queue.push({ node: node.left, column: column - 1 });
    queue.push({ node: node.right, column: column + 1 });
  }

  return getColumnOrderArr();

  function getColumnOrderArr() {
    const arr = [];
    for (let col = minCol; col <= maxCol; col++) {
      const nodeValsForThisCol = columnsToNodeVals.get(col);
      arr.push(...nodeValsForThisCol);
    }

    return arr;
  }
}

module.exports = getColumnOrderOfBinaryTree;

const SampleTrees = require('./fixtures/SampleTrees');
const getColumnOrderOfBinaryTree = require('./getColumnOrderOfBinaryTree');

describe('getColumnOrderOfBinaryTree()', () => {
  test('test on unbalanced binary tree 1', () => {
    const rootNode = SampleTrees.UNBALANCED_BINARY_TREE1;
    const expectedResult = [7, 3, 1, 2, 4, 9, 6];
    const actualResult = getColumnOrderOfBinaryTree(rootNode);
    expect(actualResult).toEqual(expectedResult);
  });
});

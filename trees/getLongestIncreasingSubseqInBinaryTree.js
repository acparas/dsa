function getLongestIncreasingSubseqInBinaryTree(rootNode) {
  let longestLen = 0;
  _dfs(-Infinity, 0, rootNode);
  return longestLen;

  function _dfs(prevVal, prevLen, node) {
    if (!node) {
      return;
    }

    const thisLen = node.val > prevVal ? prevLen + 1 : 1;
    longestLen = Math.max(longestLen, thisLen);
    _dfs(node.val, thisLen, node.left);
    _dfs(node.val, thisLen, node.right);
  }
}

module.exports = getLongestIncreasingSubseqInBinaryTree;

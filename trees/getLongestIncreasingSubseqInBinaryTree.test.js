const SampleTrees = require('./fixtures/SampleTrees');
const getLongestIncreasingSubseqInBinaryTree = (
  require('./getLongestIncreasingSubseqInBinaryTree')
);

describe('getLongestIncreasingSubseqInBinaryTree()', () => {
  test('unbalanced binary tree 2', () => {
    const rootNode = SampleTrees.UNBALANCED_BINARY_TREE2;
    const expectedResult = 4;
    const actualResult = getLongestIncreasingSubseqInBinaryTree(rootNode);
    expect(actualResult).toEqual(expectedResult);
  });

  test('perfect binary tree 1', () => {
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    const expectedResult = 2;
    const actualResult = getLongestIncreasingSubseqInBinaryTree(rootNode);
    expect(actualResult).toEqual(expectedResult);
  });

  test('empty tree', () => {
    const expectedResult = 0;
    const actualResult = getLongestIncreasingSubseqInBinaryTree(null);
    expect(actualResult).toEqual(expectedResult);
  });
});

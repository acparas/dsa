function getZigZagOrderOfBinaryTree(root) {
  const zigZagOrder = [];

  let leftToRight = true;
  let currentLevel = [root];

  while (currentLevel.length > 0) {
    const vals = currentLevel.map(node => node.val);
    if (!leftToRight) {
      vals.reverse();
    }

    zigZagOrder.push(...vals);

    currentLevel = currentLevel
      .flatMap(node => [node.left, node.right].filter(Boolean));

    leftToRight = !leftToRight;
  }

  return zigZagOrder;
}

module.exports = getZigZagOrderOfBinaryTree;

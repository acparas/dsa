const SampleTrees = require('./fixtures/SampleTrees');
const getZigZagOrderOfBinaryTree = require('./getZigZagOrderOfBinaryTree');

describe('getZigZagOrderOfBinaryTree()', () => {
  test('test on unbalanced binary tree 1', () => {
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    const expectedResult = [10, 20, 5, 12, 3, 7, 18, 9, 4];
    const actualResult = getZigZagOrderOfBinaryTree(rootNode);
    expect(actualResult).toEqual(expectedResult);
  });

  test('test on unbalanced binary tree 2', () => {
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE2;
    const expectedResult = [15, 23, 6, 4, 7, 71, 50, 5];
    const actualResult = getZigZagOrderOfBinaryTree(rootNode);
    expect(actualResult).toEqual(expectedResult);
  });
});

const SampleTrees = require('./fixtures/SampleTrees');
const iterativeInOrderTraverse = require('./iterativeInOrderTraverse');
const recursiveInOrderTraverse = require('./recursiveInOrderTraverse');

describe('Iterative InOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    iterativeInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      5,
      4,
      12,
      10,
      9,
      3,
      18,
      20,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    iterativeInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      9,
      5,
      18,
      10,
      3,
      20,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    iterativeInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      1,
      2,
      3,
      4,
      5,
      6
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

describe('Recursive InOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    recursiveInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      5,
      4,
      12,
      10,
      9,
      3,
      18,
      20,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    recursiveInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      9,
      5,
      18,
      10,
      3,
      20,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    recursiveInOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      1,
      2,
      3,
      4,
      5,
      6
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

function createVisitFunc(nodesVisited) {
  return function visit(node) {
    nodesVisited.push(node);
  };
}

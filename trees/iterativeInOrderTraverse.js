function inOrderTraverse(visit, rootNode) {
  const stack = [];
  let currentNode = rootNode;

  while (stack.length > 0 || currentNode) {
    if (currentNode) {
      stack.push(currentNode);
      currentNode = currentNode.left;
    } else {
      currentNode = stack.pop();
      visit(currentNode);
      currentNode = currentNode.right;
    }
  }
}

module.exports = inOrderTraverse;

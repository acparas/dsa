function postOrderTraverse(visit, rootNode) {
  const stack = [];
  let currentNode = rootNode;
  let lastVisitedNode = null;

  while (stack.length > 0 || currentNode) {
    if (currentNode) {
      stack.push(currentNode);
      currentNode = currentNode.left;
    } else {
      const peekNode = stack[stack.length - 1];
      if (peekNode.right && peekNode.right !== lastVisitedNode) {
        currentNode = peekNode.right;
      } else {
        visit(peekNode);
        lastVisitedNode = stack.pop();
      }
    }
  }
}

module.exports = postOrderTraverse;

function preOrderTraverse(visit, rootNode) {
  const stack = [rootNode];

  while (stack.length > 0) {
    const currentNode = stack.pop();
    if (!currentNode) {
      continue;
    }

    visit(currentNode);
    stack.push(currentNode.right);
    stack.push(currentNode.left);
  }
}

module.exports = preOrderTraverse;

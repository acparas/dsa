function levelOrderTraverse(visit, rootNode) {
  const queue = [rootNode];

  while (queue.length > 0) {
    const currentNode = queue.shift();
    if (!currentNode) {
      continue;
    }

    visit(currentNode);
    queue.push(currentNode.left);
    queue.push(currentNode.right);
  }
}

module.exports = levelOrderTraverse;

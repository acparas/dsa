const levelOrderTraverse = require('./levelOrderTraverse');
const SampleTrees = require('./fixtures/SampleTrees');

describe('Level-Order Traversal', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    levelOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      20,
      12,
      3,
      7,
      4,
      9,
      18
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    levelOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      20,
      9,
      18,
      3,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    levelOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      1,
      2,
      3,
      4,
      5,
      6
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

function createVisitFunc(nodesVisited) {
  return function visit(node) {
    nodesVisited.push(node);
  };
}

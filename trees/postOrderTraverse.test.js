const SampleTrees = require('./fixtures/SampleTrees');
const iterativePostOrderTraverse = require('./iterativePostOrderTraverse');
const recursivePostOrderTraverse = require('./recursivePostOrderTraverse');

describe('Iterative PostOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    iterativePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      4,
      12,
      5,
      9,
      18,
      3,
      7,
      20,
      10
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    iterativePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      9,
      18,
      5,
      3,
      7,
      20,
      10
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    iterativePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      6,
      5,
      4,
      3,
      2,
      1
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

describe('Recursive PostOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    recursivePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      4,
      12,
      5,
      9,
      18,
      3,
      7,
      20,
      10
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    recursivePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      9,
      18,
      5,
      3,
      7,
      20,
      10
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    recursivePostOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      6,
      5,
      4,
      3,
      2,
      1
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

function createVisitFunc(nodesVisited) {
  return function visit(node) {
    nodesVisited.push(node);
  };
}

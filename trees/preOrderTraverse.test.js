const SampleTrees = require('./fixtures/SampleTrees');
const iterativePreOrderTraverse = require('./iterativePreOrderTraverse');
const recursivePreOrderTraverse = require('./recursivePreOrderTraverse');

describe('Iterative PreOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    iterativePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      12,
      4,
      20,
      3,
      9,
      18,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    iterativePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      9,
      18,
      20,
      3,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    iterativePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      1,
      2,
      3,
      4,
      5,
      6
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

describe('Recursive PreOrderTraverse', () => {
  test('test on a typical binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.TYPICAL_BINARY_TREE1;
    recursivePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      12,
      4,
      20,
      3,
      9,
      18,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a perfect binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.PERFECT_BINARY_TREE1;
    recursivePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      10,
      5,
      9,
      18,
      20,
      3,
      7
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });

  test('test on a rightward-degenerate binary tree', () => {
    const nodesVisited = [];
    const visit = createVisitFunc(nodesVisited);
    const rootNode = SampleTrees.RIGHTWARD_DEGENERATE_BINARY_TREE1;
    recursivePreOrderTraverse(visit, rootNode);

    const expectedVisitOrder = [
      1,
      2,
      3,
      4,
      5,
      6
    ];

    const valsOfNodesVisited = nodesVisited.map(node => node.val);
    expect(valsOfNodesVisited).toEqual(expectedVisitOrder);
  });
});

function createVisitFunc(nodesVisited) {
  return function visit(node) {
    nodesVisited.push(node);
  };
}

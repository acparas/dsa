function inOrderTraverse(visit, node) {
  if (!node) {
    return;
  }

  inOrderTraverse(visit, node.left);
  visit(node);
  inOrderTraverse(visit, node.right);
}

module.exports = inOrderTraverse;

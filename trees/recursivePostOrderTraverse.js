function postOrderTraverse(visit, node) {
  if (!node) {
    return;
  }

  postOrderTraverse(visit, node.left);
  postOrderTraverse(visit, node.right);
  visit(node);
}

module.exports = postOrderTraverse;

function preOrderTraverse(visit, node) {
  if (!node) {
    return;
  }

  visit(node);
  preOrderTraverse(visit, node.left);
  preOrderTraverse(visit, node.right);
}

module.exports = preOrderTraverse;

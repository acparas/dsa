const Node = require('./TrieNode');

class Trie {
  constructor() {
    this._rootNode = new Node(null);
  }

  insertWord(word) {
    let node = this._rootNode;
    for (const char of word) {
      if (!node.children.has(char)) {
        node.children.set(char, new Node(char));
      }

      node = node.children.get(char);
    }

    node.isWordEnd = true;
  }

  hasWord(word) {
    const lastCharNode = this._getLastCharNodeOfStr(word);
    return Boolean(lastCharNode && lastCharNode.isWordEnd);
  }

  hasPrefix(prefix) {
    return Boolean(this._getLastCharNodeOfStr(prefix));
  }

  /* Returns the char node for the last character in str.
     If the last character doesn't have a corresponding char node,
     null is returned instead. */
  _getLastCharNodeOfStr(str) {
    let node = this._rootNode;
    for (const char of str) {
      if (!node.children.has(char)) {
        return null;
      }

      node = node.children.get(char);
    }

    return node;
  }

  deleteWord(word) {
    const deletionJobs = [];

    let node = this._rootNode;
    for (const char of word) {
      if (!node.children.has(char)) {
        return;
      }

      deletionJobs.push({
        parentNode: node,
        keyOfChildToDelete: char
      });

      node = node.children.get(char);
    }

    if (!node.isWordEnd) {
      return;
    }

    /* If this node has children, the given word is some other
       word's prefix. Thus, we cannot delete any nodes, otherwise
       we'd corrupt the other word's entry. All we can do is
       mark the current node no longer representing a word end. */
    if (node.children.size > 0) {
      node.isWordEnd = false;
      return;
    }

    /* If this block is reached, the node was a leaf node. This means that
       we can delete *some* nodes without corrupting another word's
       entry. We start from the leaf node and start pruning towards
       the root node. */
    while (deletionJobs.length > 0) {
      const { parentNode, keyOfChildToDelete } = deletionJobs.pop();
      parentNode.children.delete(keyOfChildToDelete);

      /* We cannot delete any more nodes. Otherwise, we'd corrupt
         another word's entry */
      if (parentNode.children.size > 0 || parentNode.isWordEnd) {
        break;
      }
    }
  }
}

module.exports = Trie;

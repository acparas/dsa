const Trie = require('./Trie');

describe('Trie', () => {
  describe('insertWord(word) & hasWord(word)', () => {
    test('insertWord("predict")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("prefecture")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prefecture')).toBe(false);

      trie.insertWord('prefecture');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prefecture')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("prediction")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(false);

      trie.insertWord('prediction');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("prediction"), then insertWord("predictions")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(false);
      expect(trie.hasWord('predictions')).toBe(false);

      trie.insertWord('prediction');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(true);
      expect(trie.hasWord('predictions')).toBe(false);

      trie.insertWord('predictions');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(true);
      expect(trie.hasWord('predictions')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("predict") again', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
    });

    test('insertWord("predictions"), then insertWord("prediction"), then insertWord("predict")', () => {
      const trie = new Trie();

      trie.insertWord('predictions');
      expect(trie.hasWord('predictions')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(false);
      expect(trie.hasWord('predict')).toBe(false);

      trie.insertWord('prediction');
      expect(trie.hasWord('predictions')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(true);
      expect(trie.hasWord('predict')).toBe(false);

      trie.insertWord('predict');
      expect(trie.hasWord('predictions')).toBe(true);
      expect(trie.hasWord('prediction')).toBe(true);
      expect(trie.hasWord('predict')).toBe(true);
    });
  });

  describe('hasPrefix(prefix)', () => {
    test('insertWord("predict"), then numerous hasPrefix() tests', () => {
      const trie = new Trie();
      trie.insertWord('predict');

      expect(trie.hasPrefix('p')).toBe(true);
      expect(trie.hasPrefix('pr')).toBe(true);
      expect(trie.hasPrefix('pre')).toBe(true);
      expect(trie.hasPrefix('pred')).toBe(true);
      expect(trie.hasPrefix('predi')).toBe(true);
      expect(trie.hasPrefix('predic')).toBe(true);
      expect(trie.hasPrefix('predict')).toBe(true);

      expect(trie.hasPrefix('predictable')).toBe(false);
    });
  });

  describe('deleteWord(word)', () => {
    test('insertWord("predict"), then deleteWord("predict")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasPrefix('predict')).toBe(true);

      trie.deleteWord('predict');
      expect(trie.hasWord('predict')).toBe(false);
      expect(trie.hasPrefix('predict')).toBe(false);
    });

    test('insertWord("predict"), then insertWord("prediction"), then deleteWord("prediction")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      trie.insertWord('prediction');
      expect(trie.hasWord('prediction')).toBe(true);

      trie.deleteWord('prediction');
      expect(trie.hasWord('prediction')).toBe(false);
      expect(trie.hasWord('predict')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("prediction"), then deleteWord("predict")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      trie.insertWord('prediction');
      expect(trie.hasWord('prediction')).toBe(true);

      trie.deleteWord('predict');
      expect(trie.hasWord('predict')).toBe(false);
      expect(trie.hasWord('prediction')).toBe(true);
    });

    test('insertWord("predict"), then insertWord("prediction"), then deleteWord("predict"), then deleteWord("prediction")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      trie.insertWord('prediction');
      expect(trie.hasWord('prediction')).toBe(true);

      trie.deleteWord('predict');
      expect(trie.hasWord('predict')).toBe(false);
      expect(trie.hasWord('prediction')).toBe(true);

      trie.deleteWord('prediction');
      expect(trie.hasWord('predict')).toBe(false);
      expect(trie.hasWord('prediction')).toBe(false);
    });

    test('insertWord("predict"), then deleteWord("predator")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      expect(trie.hasWord('predator')).toBe(false);

      trie.deleteWord('predator');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('predator')).toBe(false);
    });

    test('insertWord("predict"), then deleteWord("pre")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      expect(trie.hasWord('pre')).toBe(false);
      trie.deleteWord('pre');
      expect(trie.hasWord('predict')).toBe(true);
      expect(trie.hasWord('pre')).toBe(false);
    });

    test('insertWord("predict"), insertWord("predator"), then deleteWord("predator")', () => {
      const trie = new Trie();

      trie.insertWord('predict');
      expect(trie.hasWord('predict')).toBe(true);

      expect(trie.hasWord('predator')).toBe(false);
      trie.insertWord('predator');
      expect(trie.hasWord('predator')).toBe(true);

      trie.deleteWord('predator');
      expect(trie.hasWord('predator')).toBe(false);

      expect(trie.hasWord('predict')).toBe(true);
    });
  });
});

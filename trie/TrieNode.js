class TrieNode {
  constructor(data) {
    this.data = data;
    this.children = new Map();
    this.isWordEnd = false;
  }
}

module.exports = TrieNode;
